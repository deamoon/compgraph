/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTex;
uniform sampler2D diffuseTex2;
uniform int time2;
uniform float time3;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
in vec2 texCoord2;

out vec4 fragColor; //выходной цвет фрагмента

const float shininess = 128.0;

float abs(float x) {
	if (x < 0) return -x;
	return x;
}

void main()
{
	// texCoord2 = texCoord;

	// texCoord2.x += ((int)(time3) % 3000) / 3000.0;
	// if (texCoord2.x > 1) {
	// 	texCoord2.x -= 1.0;
	// }

	vec2 texCoordNew;
	vec3 diffuseColor;

	if (texCoord.y < 0.5f)
	{
		texCoordNew.y = 2 * texCoord.y;
		texCoordNew.x = texCoord.x + sin(time3);
		diffuseColor = texture(diffuseTex, texCoordNew).rgb;
	}
	else
	{
		texCoordNew.y = 2 * texCoord2.y;
		texCoordNew.x = texCoord2.x - sin(time3);
		diffuseColor = texture(diffuseTex2, texCoordNew).rgb;
	}

	// vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;
	// vec3 diffuseColor2 = texture(diffuseTex2, texCoord).rgb;

	//if (time2 / 1000 % 2 == 0) {
	// if (texCoord.x > abs(int(time3 * 300) % 3000 / 1500.0 - 1.0)) {
	// 	diffuseColor = texture(diffuseTex2, texCoord).rgb;
	// 	diffuseColor2 = texture(diffuseTex, texCoord).rgb;
	// }

	// if ( texCoord.y > sin(time3) ) {
	// 	diffuseColor = diffuseColor2;
	// }
	// diffuseColor += sin(time3);

	// sin(diffuseTex2)

	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
	
	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz); //направление на источник света	

	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	vec3 color = vec3(0, 0, 0);

	if (NdotL > 0.0)
	{			
		color = diffuseColor * (light.La + light.Ld * NdotL);
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну				
		blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		color += light.Ls * blinnTerm;
	} else {
		NdotL = max(dot(-normal, lightDirCamSpace.xyz), 0.0);
		color = diffuseColor * (light.La + light.Ld * NdotL);
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		float blinnTerm = max(dot(-normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну				
		blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		color += light.Ls * blinnTerm;
	}

	fragColor = vec4(color, 1.0); //просто копируем	
}
